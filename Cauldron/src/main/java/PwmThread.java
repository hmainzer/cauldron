import com.pi4j.wiringpi.SoftPwm;

/**
 * Created by hendrik on 25.02.2017.
 */
public class PwmThread extends Thread{
    int t_pin;
    int t_mult;

    public PwmThread(int pin, int mult){
        t_pin = pin;
    }

    @Override
    public void run() {
        while (Main.run) {
            for (int i = 0; i <= 100 && Main.run; i++) {
                SoftPwm.softPwmWrite(t_pin, i);
                try {
                    Thread.sleep(Main.randomMs(t_mult));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            for (int i = 100; i >= 0 && Main.run; i--) {
                SoftPwm.softPwmWrite(t_pin, i);
                try {
                    Thread.sleep(Main.randomMs(t_mult));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}