import com.pi4j.wiringpi.Gpio;
import com.pi4j.wiringpi.SoftPwm;
import server.SocketServer;
import util.PinMappings;

import java.io.IOException;

/**
 * Created by hendrik on 25.02.2017.
 * Deploy: Build -> Artifacts -> CauldronJar -> .bat file in KesselPi
 */
public class Main {

    public static boolean run = true;

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Server starting...");
        Gpio.wiringPiSetup();

        // test all pins 0-39
       /* for (Integer i : util.PinMappings.getInstance().nameToPin.values()) {
            int su = SoftPwm.softPwmCreate(i, 0, 100);
            System.out.println(su);
            SoftPwm.softPwmWrite(i, 100);
            Thread.sleep(100);
            SoftPwm.softPwmStop(i);
        }*/
/*
        int i = PinMappings.getInstance().nameToPin.get("C_WHITE");
        int su = SoftPwm.softPwmCreate(i, 0, 100);
        System.out.println(su);
        SoftPwm.softPwmWrite(i, 15);
        Thread.sleep(1500);
        SoftPwm.softPwmStop(i);
*/
        SocketServer s = null;
        try {
            s = new SocketServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        s.connect();
       /* Gpio.wiringPiSetup();
        SoftPwm.softPwmCreate(0, 0, 100);
        SoftPwm.softPwmCreate(2, 0, 100);
        SoftPwm.softPwmCreate(3, 0, 100);

        Thread t1 = new PwmThread(0, 1);
        Thread t2 = new PwmThread(2, 3);
        Thread t3 = new PwmThread(3, 7);

        t1.start();
        t2.start();
        t3.start();

        Thread.sleep(60000);
        run = false;
        Thread.sleep(2000);

        SoftPwm.softPwmWrite(0,0);
        SoftPwm.softPwmWrite(2,0);
        SoftPwm.softPwmWrite(3,0);
        SoftPwm.softPwmStop(0);
        SoftPwm.softPwmStop(2);
        SoftPwm.softPwmStop(3);*/
    }

    public static long randomMs(int mult) {
        return (long) ((Math.random() * 100) + 1) * mult;
    }


}
