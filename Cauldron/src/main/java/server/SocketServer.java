package server; /**
 * Created by hendrik on 27.05.2017.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.StringTokenizer;

import com.pi4j.wiringpi.SoftPwm;
import handler.SequenceHandler;
import util.PinMappings;
import util.SequenceHandlerBuilder;
import util.SequenceManager;

public class SocketServer {
    private final ServerSocket server;

    public SocketServer() throws IOException {
        server = new ServerSocket(12345);
    }

   /* private void setupPWM() {
        for (Integer i : PinMappings.getInstance().nameToPin.values()) {
            SoftPwm.softPwmCreate(i, 0, 100);
        }
    }*/
/*
    private void setPWM(String target, int pwm) {
        System.out.println("Target: " + target + " PWM: " + pwm);
        if (target.equals("DEBUG")){
            for (Integer i : PinMappings.getInstance().nameToPin.values()) {
                int su = SoftPwm.softPwmCreate(i, 0, 100);
                System.out.println(su);
                SoftPwm.softPwmWrite(i, 100);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                SoftPwm.softPwmStop(i);
            }
        } else {
            int pin = PinMappings.getInstance().nameToPin.get(target);
            System.out.println("Pin: " + pin);
            SoftPwm.softPwmWrite(pin, pwm);
        }
    }*/
/*
    private void stopPWM() {
        for (Integer i : PinMappings.getInstance().nameToPin.values()) {
            SoftPwm.softPwmStop(i);
        }
    }*/

    public void connect() {
        System.out.println("connect...");
        while (true) {
            Socket socket = null;
            //setupPWM();
            try {
                System.out.println("ready...");
                PinMappings.getInstance().getHandler("C_RED").setPercent(10, this);
                socket = server.accept();
                System.out.println("accepted...");
                PinMappings.getInstance().getHandler("C_RED").setOff(this, true);
                //receive(socket);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String s;
                while ((s = in.readLine()) != null) {
                    System.out.println(s);
                    processCommand(s);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (socket != null)
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                //stopPWM();
            }
        }
    }

    private void receive(Socket socket) throws IOException {
        BufferedReader incoming = new BufferedReader(new InputStreamReader(socket
                .getInputStream()));
        String s;

        System.out.println("read...");

        while (incoming.ready()) {
            s = incoming.readLine();
            System.out.println(s);
        }
    }

    private void processCommand(final String command) {
        final Map.Entry<String, String> cmd = getCommandType(command);
        switch (cmd.getKey()) {
            case "STOP": {
                String handle = cmd.getValue();
                if (handle.contains(":")) {
                    handle = handle.substring(0, handle.indexOf(':'));
                }
                SequenceManager.getInstance().stopExistingHandles(handle);
                return;
            }
        }
        final SequenceHandlerBuilder builder = new SequenceHandlerBuilder(command);
        final SequenceHandler executableCommand = builder.build();
        SequenceManager.getInstance().executeSequenceHandler(executableCommand, builder.getSetZero());
    }

    public static Map.Entry<String, String> getCommandType(final String command) {
        StringTokenizer t = new StringTokenizer(command, ";");
        String token;

        if (t.hasMoreTokens()) {
            token = t.nextToken();
            StringTokenizer t2 = new StringTokenizer(command, "#");
            String token2;
            final String cmd = t2.nextToken();
            String hdl = t2.nextToken();
            final String handle;
            if (hdl.contains(";")) {
                handle = hdl.substring(0, hdl.indexOf(';'));
            } else {
                handle = hdl;
            }
            return new Map.Entry<String, String>() {
                public String getKey() {
                    return cmd;
                }

                public String getValue() {
                    return handle;
                }

                public String setValue(String value) {
                    return null;
                }
            };
        }
        return null;
    }

}
