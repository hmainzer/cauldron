package util;

import handler.SequenceHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hendrik on 21.07.2017.
 */
public class SequenceManager {

    private static SequenceManager instance = null;
    private Map<String, SequenceHandler> basicHandlers;
    private Map<String, SequenceHandler> forcedHandlers;
    private List<SequenceHandler> allRunningThreads = new ArrayList<>();

    private SequenceManager() {
        basicHandlers = new HashMap<String, SequenceHandler>();
        forcedHandlers = new HashMap<String, SequenceHandler>();
    }

    public static SequenceManager getInstance() {
        if (instance == null) {
            instance = new SequenceManager();
        }
        return instance;
    }

    public void executeSequenceHandler(final SequenceHandler handler, final boolean setPreviousToZero) {
        System.out.println("hdl1");
        final List<SequenceHandler> stoppedHandlers;
        if (!handler.isForceClaim()) {
            stoppedHandlers = stopExistingHandles(handler.getPinHandle(), basicHandlers, setPreviousToZero);
            basicHandlers.put(handler.getPinHandle(), handler);
        } else {
            stoppedHandlers = stopExistingHandles(handler.getPinHandle(), forcedHandlers, setPreviousToZero);
            forcedHandlers.put(handler.getPinHandle(), handler);
        }
        System.out.println("hdl2");
        final Runnable r = new Runnable() {
            public void run() {
                System.out.println("hdlR1");
                for (final SequenceHandler sh : stoppedHandlers) {
                    try {
                        sh.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("hdlR2");
                handler.start();
            }
        };
        (new Thread(r)).start();
    }

    private List<SequenceHandler> stopExistingHandles(String pin_handle, Map<String, SequenceHandler> handlers, boolean setZero) {
        System.out.println("Stop: " + pin_handle);//%%
        System.out.println(handlers);//%%
        List<SequenceHandler> stoppedHandlers = new ArrayList<SequenceHandler>();
        SequenceHandler sh = handlers.get(pin_handle);
        if (sh != null) {
            sh.stop(setZero);
            stoppedHandlers.add(sh);
            handlers.remove(pin_handle);
        }
        System.out.println(pin_handle);
        for (String handle : PinMappings.getInstance().getChildrensOfHandle(pin_handle)) {
            sh = handlers.get(handle);
            if (sh != null) {
                sh.stop(setZero);
                stoppedHandlers.add(sh);
                handlers.remove(handle);
            }
        }
        return stoppedHandlers;
    }

    public void stopExistingHandles(String pin_handle) {
        stopExistingHandles(pin_handle, true);
    }

    public void stopExistingHandles(String pin_handle, boolean setZero) {
        stopExistingHandles(pin_handle, basicHandlers, setZero);
        stopExistingHandles(pin_handle, forcedHandlers, setZero);
        if (pin_handle.equals("ALL")) {
            for (SequenceHandler sh : allRunningThreads) {
                sh.stop(setZero);
            }
            allRunningThreads.clear();
        }
    }

    public void registerThread(SequenceHandler thread) {
        allRunningThreads.add(thread);
    }

    public void unregisterThread(SequenceHandler thread) {
        allRunningThreads.remove(thread);
    }

}
