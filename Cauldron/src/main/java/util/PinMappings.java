package util;

import handler.PinOutHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hendrik on 27.05.2017.
 */
public class PinMappings {

    /*
        Pi Controller (Board Pin | Pi Pin | Identifier)
            1 - 11 - C_RED
            2 - 13 - C_GREEN
            3 - 15 - C_BLUE
            4 - 7 - C_WHITE
            5 - 12 - C_P1
            6 - 16 - C_P2
            7 - 18 - C_EXT
            8 - 22 - F_RED
            9 - 29 - F_GREEN
            10 - 31 - F_BLUE

         cauldron:
                col | python | wiring pi
                red     : 11    : 0
                green   : 13    : 2
                blue    : 15    : 3
                white   : 7     : 7





     */

    public Map<String, Integer> nameToPin;
    private final Map<String, PinOutHandler> handlers;
    private final Map<String, String[]> pin_handles_tree;
    private final Map<String, String[]> pin_handles;
    public final static String[] ALL = {"Cauldron", "C_LIGHT", "C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_UTIL", "C_PUMPS", "C_PUMP1", "C_PUMP2", "C_EXT", "FIRE", "F_RED", "F_GREEN", "F_BLUE"};
    public final static String[] ALL_PINS = {"C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_PUMP1", "C_PUMP2", "C_EXT", "F_RED", "F_GREEN", "F_BLUE"};
    public final static String[] CAULDRON = {"C_LIGHT", "C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_UTIL", "C_PUMPS", "C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] CAULDRON_PINS = {"C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] C_LIGHT = {"C_RED", "C_BLUE", "C_GREEN", "C_WHITE"};
    public final static String[] C_RED = {"C_RED"};
    public final static String[] C_BLUE = {"C_BLUE"};
    public final static String[] C_GREEN = {"C_GREEN"};
    public final static String[] C_WHITE = {"C_WHITE"};
    public final static String[] C_UTIL = {"C_PUMPS", "C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] C_UTIL_PINS = {"C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] C_PUMPS = {"C_PUMP1", "C_PUMP2"};
    public final static String[] C_PUMP1 = {"C_PUMP1"};
    public final static String[] C_PUMP2 = {"C_PUMP2"};
    public final static String[] C_EXT = {"C_EXT"};
    public final static String[] FIRE = {"F_RED", "F_GREEN", "F_BLUE"};
    public final static String[] F_RED = {"F_RED"};
    public final static String[] F_BLUE = {"F_GREEN"};
    public final static String[] F_GREEN = {"F_BLUE"};


    private PinMappings() {
        nameToPin = new HashMap<String, Integer>();
        nameToPin.put("C_RED", 0);
        nameToPin.put("C_GREEN", 2);
        nameToPin.put("C_BLUE", 3);
        nameToPin.put("C_WHITE", 7);
        nameToPin.put("C_PUMP1", 1);
        nameToPin.put("C_PUMP2", 4);
        nameToPin.put("C_EXT", 5);
        nameToPin.put("F_RED", 6);
        nameToPin.put("F_GREEN", 21);
        nameToPin.put("F_BLUE", 22);

        handlers = new HashMap<String, PinOutHandler>();
        for (Map.Entry<String, Integer> e : nameToPin.entrySet()) {
            handlers.put(e.getKey(), new PinOutHandler(e.getValue(), 100));
        }

        pin_handles_tree = new HashMap<String, String[]>();
        pin_handles_tree.put("ALL", ALL);
        pin_handles_tree.put("CAULDRON", CAULDRON);
        pin_handles_tree.put("C_LIGHT", C_LIGHT);
        pin_handles_tree.put("C_RED", C_RED);
        pin_handles_tree.put("C_BLUE", C_BLUE);
        pin_handles_tree.put("C_GREEN", C_GREEN);
        pin_handles_tree.put("C_WHITE", C_WHITE);
        pin_handles_tree.put("C_UTIL", C_UTIL);
        pin_handles_tree.put("C_PUMPS", C_PUMPS);
        pin_handles_tree.put("C_PUMP1", C_PUMP1);
        pin_handles_tree.put("C_PUMP2", C_PUMP2);
        pin_handles_tree.put("C_EXT", C_EXT);
        pin_handles_tree.put("FIRE", FIRE);
        pin_handles_tree.put("F_RED", F_RED);
        pin_handles_tree.put("F_BLUE", F_BLUE);
        pin_handles_tree.put("F_GREEN", F_GREEN);

        pin_handles = new HashMap<String, String[]>();
        pin_handles.put("ALL", ALL_PINS);
        pin_handles.put("CAULDRON", CAULDRON_PINS);
        pin_handles.put("C_LIGHT", C_LIGHT);
        pin_handles.put("C_RED", C_RED);
        pin_handles.put("C_BLUE", C_BLUE);
        pin_handles.put("C_GREEN", C_GREEN);
        pin_handles.put("C_WHITE", C_WHITE);
        pin_handles.put("C_UTIL", C_UTIL_PINS);
        pin_handles.put("C_PUMPS", C_PUMPS);
        pin_handles.put("C_PUMP1", C_PUMP1);
        pin_handles.put("C_PUMP2", C_PUMP2);
        pin_handles.put("C_EXT", C_EXT);
        pin_handles.put("FIRE", FIRE);
        pin_handles.put("F_RED", F_RED);
        pin_handles.put("F_BLUE", F_BLUE);
        pin_handles.put("F_GREEN", F_GREEN);
    }

    private static PinMappings singleton;

    public static PinMappings getInstance() {
        if (singleton == null) {
            singleton = new PinMappings();
        }
        return singleton;
    }

    public PinOutHandler getHandler(String pinName) {
        return handlers.get(pinName);
    }

    public String[] getChildrensOfHandle(String handle) {
        return pin_handles_tree.get(handle);
    }

    public String[] getPinsOfHandle(String handle) {
        return pin_handles.get(handle);
    }
}

