package util;

/**
 * Created by hendrik on 19.07.2017.
 */
public interface IntValue {

    public int getNumber();
}
