package util;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by hendrik on 19.07.2017.
 */
public class PulseData {

    public final IntValue lowerBound;
    public final IntValue upperBound;
    public final IntValue stepSize;
    public final IntValue stepDelay;
    public final String pin;
    public final boolean forceClaim;
    public final Integer maxPwm;
    public final Map<String, Integer> pins; // <Pin, tmpPwmMax> negative value for no adjustment
    public final boolean isMultiple;

    public PulseData(final String pin, final IntValue lowerBound, final IntValue upperBound, final IntValue stepSize, final IntValue stepDelay, final Integer maxPwm, final boolean forceClaim) {
        this.pin = pin;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.stepSize = stepSize;
        this.stepDelay = stepDelay;
        this.forceClaim = forceClaim;
        this.maxPwm = maxPwm;
        pins = new HashMap<String, Integer>();
        pins.put(pin, maxPwm);
        isMultiple = false;
    }

    public PulseData(final String pin_handle, final Map<String, Integer> pins, final IntValue lowerBound, final IntValue upperBound, final IntValue stepSize, final IntValue stepDelay, final boolean forceClaim) {
        this.pins = pins;
        this.pin = pin_handle;
        this.lowerBound = lowerBound;
        this.upperBound = upperBound;
        this.stepSize = stepSize;
        this.stepDelay = stepDelay;
        this.forceClaim = forceClaim;
        this.maxPwm = null;
        isMultiple = true;
    }
}
