package util;

/**
 * Created by hendrik on 19.07.2017.
 */
public class FixNumber implements IntValue {
    private final int number;

    public FixNumber(int i) {
        number = i;
    }

    public int getNumber() {
        return number;
    }
}
