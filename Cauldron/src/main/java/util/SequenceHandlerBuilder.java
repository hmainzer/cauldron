package util;

import handler.*;
import server.SocketServer;

import java.util.*;

/**
 * Created by hendrik on 21.07.2017.
 */
public class SequenceHandlerBuilder {
    private final String command;

    public SequenceHandlerBuilder(final String command) {
        this.command = command;
    }

    public SequenceHandler build() {
        final Map.Entry<String, String> cmd = SocketServer.getCommandType(command);
        switch (cmd.getKey()) {
            case "SET": {
                return buildSet(cmd);
            }
            case "PULSE": {
                return buildPulse(cmd, false);
            }
            case "BUILDUP": {
                return buildPulse(cmd, true);
            }
            case "STROBE": {
                return buildStrobe(cmd);
            }
        }
        return null;
    }

    private SequenceHandler buildSet(final Map.Entry<String, String> cmd) {
        String[] handles = PinMappings.getInstance().getPinsOfHandle(cmd.getValue());
        ParallelSequence parent = null;
        final boolean force = command.contains("DO_FORCE");
        if (handles.length > 1) {
            parent = new ParallelSequence(cmd.getValue(), force);
        }
        StringTokenizer t = new StringTokenizer(command, ";");
        t.nextToken(); // skip Command def
        String token = t.nextToken();
        final int pwm;
        if (token.contains("ON")) {
            pwm = 100;
        } else if (token.contains("PWM:")) {
            t = new StringTokenizer(token, ":");
            t.nextToken(); // skip "PWM"
            pwm = Integer.parseInt(t.nextToken());
            System.out.println("pwm: " + pwm);
        } else {
            pwm = 0;
        }
        System.out.println("pwm: " + pwm);
        for (String handle : handles) {
            final StaticHandler sh = new StaticHandler(handle, pwm, force);
            sh.stop(false);
            if (parent != null) {
                parent.addSequence(sh);
            } else {
                return sh;
            }
        }
        return parent;
    }

    private SequenceHandler buildPulse(final Map.Entry<String, String> cmd, final boolean buildup) {

        String[] handles = PinMappings.getInstance().getPinsOfHandle((String) cmd.getValue());
        boolean doForce = command.contains("DO_FORCE");
        boolean doTransit = command.contains("DO_TRANSIT");
        boolean setZero = command.contains("DO_SETZERO");
        boolean hasTimer = command.contains("TIMER(");
        long timer;
        if (hasTimer) {
            int idx = command.indexOf("TIMER(") + 6;
            String timeString = command.substring(idx, command.indexOf(')', idx));
            timer = Long.parseLong(timeString);
        } else {
            timer = -1L;
        }
        List<PulseData> pulseData = new java.util.ArrayList();
        String workString = command;
        Map<String, Integer> maxPwmMap = new java.util.HashMap();
        if (workString.contains("MAX_PWM")) {
            String maxPwmString = workString.substring(workString.indexOf("MAX_PWM"));
            while (maxPwmString.contains("MAX_PWM")) {
                maxPwmString = maxPwmString.substring(maxPwmString.indexOf("MAX_PWM#") + 8);
                int idx = maxPwmString.indexOf(':');
                String pinHdl = maxPwmString.substring(0, idx);
                int idx2 = maxPwmString.indexOf(';');
                int max;
                if (idx2 >= 0) {
                    max = Integer.parseInt(maxPwmString.substring(idx + 1, idx2));
                } else {
                    max = Integer.parseInt(maxPwmString.substring(idx + 1));
                }
                maxPwmMap.put(pinHdl, Integer.valueOf(max));
            }
        }
        while (workString.contains("PULSEDATA")) {
            workString = workString.substring(workString.indexOf("PULSEDATA#") + 10);
            int idx = workString.indexOf(';');
            String pulseString;
            if (idx >= 0) {
                pulseString = workString.substring(0, idx);
            } else {
                pulseString = workString;
            }
            int idx2 = workString.indexOf('(');
            String handle = workString.substring(0, idx2);
            String[] subhandles = PinMappings.getInstance().getPinsOfHandle(handle);
            StringTokenizer t = new StringTokenizer(workString.substring(idx2 + 1), ",");
            IntValue low = parseValue(t.nextToken());
            IntValue high = parseValue(t.nextToken());
            IntValue step = parseValue(t.nextToken());
            IntValue delay = parseValue(t.nextToken());
            PulseData pd = null;
            if (subhandles.length > 1) {
                Map<String, Integer> pins = new java.util.HashMap();
                for (String hdl : subhandles) {
                    pins.put(hdl, maxPwmMap.get(hdl));
                }
                pd = new PulseData(handle, pins, low, high, step, delay, doForce);
            } else {
                pd = new PulseData(subhandles[0], low, high, step, delay, (Integer) maxPwmMap.get(subhandles[0]), doForce);
            }
            pulseData.add(pd);
        }

        // analyse and decide for handler
        int pins = handles.length;
        int pulses = pulseData.size();

        /**
         * Options:
         * PulseHandler -> Single Pulse
         * TimedPulseHandler -> Single Pulse + Timer
         * SynchronMultiPulseHandler -> Multiple Pins, One Pulse
         * Timed... ^^ + Timer
         * MultiPulseHandler -> Multi Pins, Multi Pulse
         * TimedMult...
         * */

        if (pins == 1) {
            if (hasTimer) {
                System.out.println("TPH");
                return new handler.TimedPulseHandler((PulseData) pulseData.get(0), doTransit, timer);
            }
            System.out.println("PH");
            return new handler.PulseHandler((PulseData) pulseData.get(0), doTransit);
        }

        if (pulses > 1) {
            if (hasTimer) {
                System.out.println("TMPH");
                return new handler.TimedMultiPulseHandler(doTransit, (String) cmd.getValue(), timer, (PulseData[]) pulseData.toArray(new PulseData[0]));
            }
            System.out.println("MPH");
            return new handler.MultiPulseHandler(doTransit, (String) cmd.getValue(), (PulseData[]) pulseData.toArray(new PulseData[0]));
        }

        if (hasTimer) {
            System.out.println("TSMPPH");
            return new handler.TimedSynchronMultiPinPulseHandler((PulseData) pulseData.get(0), doTransit, timer);
        }
        System.out.println("SMPPH");
        return new handler.SynchronMultiPinPulseHandler((PulseData) pulseData.get(0), doTransit);
    }

    private SequenceHandler buildStrobe(final Map.Entry<String, String> cmd) {
        final String[] handles = PinMappings.getInstance().getPinsOfHandle(cmd.getValue());
        final boolean doForce = command.contains("DO_FORCE");
        final boolean doTransit = command.contains("DO_TRANSIT");
        final boolean setZero = command.contains("DO_SETZERO");
        final boolean hasTimer = command.contains("TIMER(");
        final long timer;
        if (hasTimer) {
            int idx = command.indexOf("TIMER(") + 6;
            String timeString = command.substring(idx, command.indexOf(')', idx));
            timer = Long.parseLong(timeString);
        } else {
            timer = -1;
        }
        List<StroboData> strobeData = new ArrayList<>();
        String workString = command;
        Map<String, Integer> maxPwmMap = new HashMap<>();
        if (workString.contains("MAX_PWM")) {
            String maxPwmString = workString.substring(workString.indexOf("MAX_PWM"));
            while (maxPwmString.contains("MAX_PWM")) {
                maxPwmString = maxPwmString.substring(maxPwmString.indexOf("MAX_PWM#") + 8);
                int idx = maxPwmString.indexOf(':');
                String pinHdl = maxPwmString.substring(0, idx);
                int idx2 = maxPwmString.indexOf(';');
                int max;
                if (idx2 >= 0) {
                    max = Integer.parseInt(maxPwmString.substring(idx + 1, idx2));
                } else {
                    max = Integer.parseInt(maxPwmString.substring(idx + 1));
                }
                maxPwmMap.put(pinHdl, max);
            }
        }
        while (workString.contains("STROBEDATA")) {
            System.out.println(workString);
            workString = workString.substring(workString.indexOf("STROBEDATA#") + 11);
            final int idx = workString.indexOf(';');
            final String strobeString;
            if (idx >= 0) {
                strobeString = workString.substring(0, idx);
            } else {
                strobeString = workString;
            }
            System.out.println(strobeString);
            int idx2 = strobeString.indexOf('(');
            String handle = strobeString.substring(0, idx2);
            StringTokenizer t = new StringTokenizer(strobeString.substring(idx2 + 1), ",");
            IntValue high = parseValue(t.nextToken());
            IntValue flash = parseValue(t.nextToken());
            IntValue delay = parseValue(t.nextToken());
            StroboData sd = null;

            sd = new StroboData(handle, strobeString.contains("DO_LOWZERO"), high, flash, delay, maxPwmMap.get(handle), doForce);

            strobeData.add(sd);
        }

        // analyse and decide for handler
        final int strobes = strobeData.size();

        /**
         * Options:
         * PulseHandler -> Single Pulse
         * TimedPulseHandler -> Single Pulse + Timer
         * SynchronMultiPulseHandler -> Multiple Pins, One Pulse
         * Timed... ^^ + Timer
         * MultiPulseHandler -> Multi Pins, Multi Pulse
         * TimedMult...
         * */

        if (strobes == 1) {
            if (hasTimer) {
                System.out.println("TSH");
                return new TimedStroboHandler(strobeData.get(0), timer);
            } else {
                System.out.println("SH");
                return new StroboHandler(strobeData.get(0));
            }
        } else {
            if (hasTimer) {
                System.out.println("PS-TSH");
                ParallelSequence ps = new ParallelSequence(cmd.getValue(), doForce);
                for (StroboData sd : strobeData) {
                    ps.addSequence(new TimedStroboHandler(sd, timer));
                }
                return ps;
            } else {
                System.out.println("PS-SH");
                ParallelSequence ps = new ParallelSequence(cmd.getValue(), doForce);
                for (StroboData sd : strobeData) {
                    ps.addSequence(new StroboHandler(sd));
                }
                return ps;
            }
        }
    }

    public boolean getSetZero() {
        return true;
    }

    private IntValue parseValue(String valueText) {
        // either RND(base. dice...) or VAL(x)
        if (valueText.contains("VAL(")) {
            return new FixNumber(Integer.parseInt(valueText.substring(4, valueText.indexOf(')'))));
        }
        if (valueText.contains("RND(")) {
            final String values = valueText.substring(4, valueText.indexOf(')'));
            StringTokenizer t = new StringTokenizer(values, ".");
            int base = Integer.parseInt(t.nextToken());
            List<Integer> dice = new ArrayList<>();
            while (t.hasMoreTokens()) {
                dice.add(Integer.parseInt(t.nextToken()));
            }
            boolean inverse = valueText.contains("INVERSE");
            return new RandomGenerator(base, inverse, dice.toArray(new Integer[0]));
        }
        return null;
    }
}
