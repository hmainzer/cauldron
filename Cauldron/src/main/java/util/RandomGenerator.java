package util;

import java.util.*;

/**
 * Created by hendrik on 19.07.2017.
 */
public class RandomGenerator implements IntValue {

    private final int baseNumber;
    private final List<Integer> dice;
    private final Random r;
    private final Map<Integer, Integer> probabilityMapping;

    public RandomGenerator(final int base, final Integer... dice) {
        this.baseNumber = base;
        this.dice = new ArrayList<Integer>(Arrays.asList(dice));
        r = new Random();
        probabilityMapping = null;
    }

    public RandomGenerator(final int base, final boolean inverse, final Integer... dice) {
        this.baseNumber = base;
        this.dice = new ArrayList<Integer>(Arrays.asList(dice));
        r = new Random();
        if (dice.length <= 1 || inverse == false) {
            probabilityMapping = null;
        } else {
            probabilityMapping = new HashMap<>();
            int min = dice.length;
            int max = 0;
            for (int i : dice) {
                max += i;
            }
            if (inverse) {
                boolean up = false;
                int j = (min - 1) + (int) Math.ceil((max - (min - 1)) / 2);
                for (int i = min; i <= max; i++) {
                    probabilityMapping.put(i, j);
                    if (j == min) {
                        j = max +1;
                    }
                    j--;
                }
            }
        }
    }

    public int getNumber() {
        int number = 0;
        for (Integer i : dice) {
            number += (r.nextInt(i) + 1);
        }
        if (probabilityMapping != null) {
            return probabilityMapping.get(number) + baseNumber;
        }
        return number + baseNumber;
    }

}
