package util;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hendrik on 19.07.2017.
 */
public class StroboData {
    public final boolean forceLowZero;
    public final IntValue upperBound;
    public final IntValue flashTime;
    public final IntValue delayTime;
    public final String pin;
    public final boolean forceClaim;
    public final Integer maxPwm;

    public StroboData(final String pin, final boolean forceLowZero, final IntValue upperBound, final IntValue flashTime, final IntValue delayTime, final Integer maxPwm, final boolean forceClaim) {
        this.pin = pin;
        this.forceLowZero = forceLowZero;
        this.upperBound = upperBound;
        this.flashTime = flashTime;
        this.delayTime = delayTime;
        this.forceClaim = forceClaim;
        this.maxPwm = maxPwm;
    }
}
