package handler;

import util.PinMappings;

/**
 * Created by hendrik on 19.07.2017.
 */
public class StaticHandler extends SequenceHandler {
    private final PinOutHandler pinHandler;
    private final boolean forceClaim;
    private int pwm;

    public StaticHandler(final String pin, final int startPwm, final boolean forceClaim) {
        super(pin, forceClaim);
        pinHandler = PinMappings.getInstance().getHandler(pin);
        this.forceClaim = forceClaim;
        pwm = startPwm;
        System.out.println("pwm: " + pwm);
    }

    @Override
    public void run() {
        registerThread();
        if (forceClaim) {
            pinHandler.forceClaim(this);
        }
        do {
            pinHandler.setPercent(pwm, this);
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } while (!checkForStop());
        if (setZero) {
            pinHandler.setPercent(0, this);
        }
        pinHandler.release(this);
        unregisterThread();
    }

    protected boolean checkForStop() {
        return stop;
    }

    public void changePwm(final int newPwm) {
        pwm = newPwm;
    }
}
