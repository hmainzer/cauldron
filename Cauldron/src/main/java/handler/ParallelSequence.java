package handler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hendrik on 19.07.2017.
 * some LineSequences executed in parallel in separate Threads
 */
public class ParallelSequence extends SequenceHandler implements SelfEndingHandler {
    private final List<SequenceHandler> sequences;

    public ParallelSequence(String pin_handle, boolean force) {
        super(pin_handle,force);
        sequences = new ArrayList<SequenceHandler>();
    }

    public void addSequence(final SequenceHandler sequence) {
        sequences.add(sequence);
    }

    @Override
    public void run() {
        registerThread();
        final List<Thread> threads = new ArrayList<Thread>();
        for (final SequenceHandler s : sequences) {
            s.start();
            threads.add(s);
        }
        for (Thread t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        unregisterThread();
    }

    public void h_start() {
        this.start();
    }

    public void h_stop(boolean setZero) {
        this.stop(setZero);
    }

    public void h_join() throws InterruptedException {
        this.join();
    }

    public void h_join(long miliseconds) throws InterruptedException {
        this.join(miliseconds);
    }

    @Override
    public void stop(boolean setZero) {
        super.stop(setZero);
        for (final SequenceHandler s : sequences) {
            s.stop(setZero);
        }
    }
}
