package handler;

/**
 * Created by hendrik on 19.07.2017.
 */
public class JustWaitHandler extends SequenceHandler {
    private final long runtime;

    public JustWaitHandler(String pin_handle, final long runtime){
        super(pin_handle, false);
        this.runtime = runtime;
    }

    @Override
    public void run() {
        registerThread();
        try {
            Thread.sleep(runtime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        unregisterThread();
    }
}
