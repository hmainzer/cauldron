package handler;

import handler.PinOutHandler;
import handler.PulseHandler;
import handler.SequenceHandler;
import util.PinMappings;
import util.PulseData;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by hendrik on 19.07.2017.
 * <p>
 * multiple Pins, one delay, one percent giver, only maxPwm per Pin may differ
 */
public class SynchronMultiPinPulseHandler extends SequenceHandler {
    private final PulseData pulse;
    private final boolean transistion;
    private final List<PinOutHandler> pinHandler;

    public SynchronMultiPinPulseHandler(final PulseData pulse, final boolean transition) {
        super(pulse.pin, pulse.forceClaim);
        this.pulse = pulse;
        this.transistion = transition;
        pinHandler = new ArrayList<PinOutHandler>();
        for (final Map.Entry<String, Integer> e : pulse.pins.entrySet()) {
            final PinOutHandler handler = PinMappings.getInstance().getHandler(e.getKey());
            if (e.getValue() != null) {
                handler.setMaxForUserPercent(e.getValue(), this);
            }
            pinHandler.add(handler);
        }
    }

    public void run() {
        registerThread();
        if (pulse.forceClaim) {
            for (PinOutHandler handler : pinHandler) {
                handler.forceClaim(this);
            }
        }
        int currentPwm = pulse.lowerBound.getNumber();
        if (transistion) {
            boolean makeTransistion = true;
            final int delay = pulse.stepDelay.getNumber();
            while (makeTransistion) {
                int i = 0;
                for (PinOutHandler handler : pinHandler) {
                    int current = handler.getPwm();
                    if (current > currentPwm) {
                        current = current - 5;
                    } else {
                        current = current + 5;
                    }
                    final boolean access = handler.setPercent(current, this);
                    if (Math.abs(current - currentPwm) < 10 || !access) {
                        i++;
                    }
                }
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (i++ >= pinHandler.size()) {
                    makeTransistion = false;
                }
            }

        }
        int target = pulse.upperBound.getNumber();
        boolean up = false;
        while (!checkForStop()) {
            currentPwm = progressToTarget(currentPwm, target);
            if (up) {
                target = pulse.upperBound.getNumber();
            } else {
                target = pulse.lowerBound.getNumber();
            }
            up = !up;
            doStopConditionStuff();
        }
        for (PinOutHandler handler : pinHandler) {
            handler.setOff(this);
            handler.release(this);
        }
        unregisterThread();
    }

    protected boolean checkForStop() {
        return stop;
    }

    protected void doStopConditionStuff() {
        return;
    }

    private int progressToTarget(int current, final int target) {
        if (current == target) {
            return current;
        }
        final int stepPositive = pulse.stepSize.getNumber();
        final int step = stepPositive * (current < target ? 1 : -1);
        final int runtime = Math.abs(current - target);
        int runtimeCounter = 0;
        final int delay = pulse.stepDelay.getNumber();
        while (runtimeCounter < runtime && !checkForStop()) {
            runtimeCounter += stepPositive;
            current = current + step;
            for (PinOutHandler handler : pinHandler) {
                handler.setPercent(current, this);
            }
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return current;
    }
}
