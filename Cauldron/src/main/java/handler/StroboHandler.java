package handler;

import util.PinMappings;
import util.StroboData;

/**
 * Created by hendrik on 19.07.2017.
 * turn power on to upper bound an then reduce to zero for a rectangular spike pattern
 */
public class StroboHandler extends SequenceHandler {
    private final StroboData strobo;
    private final PinOutHandler pinHandler;

    public StroboHandler(final StroboData strobo) {
        super(strobo.pin, strobo.forceClaim);
        this.strobo = strobo;
        pinHandler = PinMappings.getInstance().getHandler(strobo.pin);
        if (strobo.maxPwm != null) {
            pinHandler.setMaxForUserPercent(strobo.maxPwm, this);
        }
    }

    public void run() {
        registerThread();
        if (strobo.forceClaim) {
            pinHandler.forceClaim(this);
        }
        while (!checkForStop()) {
            if (strobo.forceClaim) {
                pinHandler.forceClaim(this);
            }
            pinHandler.setPercent(strobo.upperBound.getNumber(), this);
            try {
                Thread.sleep(strobo.flashTime.getNumber());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!checkForStop()) {
                pinHandler.setOff(this, !strobo.forceLowZero);
                try {
                    Thread.sleep(strobo.delayTime.getNumber());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        if (setZero) {
            pinHandler.setOff(this);
        }
        pinHandler.release(this);
        unregisterThread();
    }

    protected boolean checkForStop() {
        return stop;
    }
}
