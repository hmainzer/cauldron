package handler;

import com.pi4j.wiringpi.SoftPwm;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hendrik on 19.07.2017.
 */
public class PinOutHandler {
    private final int pin;
    private final int pwmMax;
    private final int pwmPercent;
    private final Map<Object, Integer> tmpPwmMax = new HashMap<Object, Integer>();
    private final Map<Object, Integer> tmpPwmPercent = new HashMap<Object, Integer>();

    private int currentPwm;

    private Object claimer = null;
    private Object currentUser = null;
    public static final int MAX_PWM = 100;

    public PinOutHandler(final int pin, final int pwmMax) {
        this.pin = pin;
        this.pwmMax = pwmMax;
        pwmPercent = Math.round(pwmMax / MAX_PWM);
        currentPwm = 0;
        SoftPwm.softPwmCreate(pin, 0, pwmMax);
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        try {
            SoftPwm.softPwmStop(pin); // sollte nicht benötigt werden, solange server nicht gestoppt wird
        } catch (RuntimeException e) {
        }
    }

    private synchronized boolean checkAndSetOwnage(final Object o) {
       // System.out.println("checkAndSetOwnage");
        if (claimer != null) {
            if (o.equals(claimer)) {
                return true;
            } else {
                return false;
            }
        }
        if (currentUser != null) {
            if (o.equals(currentUser)) {
                return true;
            } else {
                return false;
            }
        }
        currentUser = o;
        return true;
    }

    public synchronized boolean forceClaim(final Object o) {
       // System.out.println("ForceClaim");
        if (o == null) {
            return false;
        }
        if (claimer == null) {
            this.claimer = o;
            return true;
        }
        if (o.equals(claimer)) {
            return true;
        }
        return false;
    }

    public synchronized void release(final Object o) {
       // System.out.println("release");
        if (o.equals(claimer)) {
            claimer = null;
        }
        if (o.equals(currentUser)) {
            currentUser = null;
        }
    }

    public boolean setOff(final Object o) {
        return setOff(o, false);
    }

    public boolean setOff(final Object o, final boolean clearClaim) {
       // System.out.println("setOff");
        if (!checkAndSetOwnage(o)) {
            return false;
        }
        if (clearClaim) {
            release(o);
        }
        currentPwm = 0;
        SoftPwm.softPwmWrite(pin, currentPwm);
        return true;
    }

    public boolean setOn(final Object o) {
       // System.out.println("setOn");
        if (!checkAndSetOwnage(o)) {
            return false;
        }
        currentPwm = pwmMax;
        SoftPwm.softPwmWrite(pin, currentPwm);
        return true;
    }

    public boolean setPwm(final int pwm, final Object o) {
       // System.out.println("setPwm");
        if (!checkAndSetOwnage(o)) {
            return false;
        }
        currentPwm = pwm;
        SoftPwm.softPwmWrite(pin, currentPwm);
        return true;
    }

    public boolean setPercent(final int percent, final Object o) {
       // System.out.println("setPercent");
        if (!checkAndSetOwnage(o) && percent <= MAX_PWM) {
            return false;
        }
       // System.out.println("setPercent2: percent " + pwmPercent);
        if (tmpPwmMax.containsKey(o)) {
            currentPwm = percent * tmpPwmPercent.get(o);
        } else {
            currentPwm = percent * pwmPercent;
        }
       // System.out.println("setPercent3: " + pin + " -> " + currentPwm);
        SoftPwm.softPwmWrite(pin, currentPwm);
        return true;
    }

    public void setMaxForUserPercent(int tmpMaxPwm, final Object o) {
       // System.out.println("setMxUsrPerc");
        if (!checkAndSetOwnage(o)) {
            return;
        }
        if (tmpMaxPwm > MAX_PWM || tmpMaxPwm <= 0) {
            tmpMaxPwm = MAX_PWM;
        }
        tmpPwmMax.put(o, tmpMaxPwm);
        tmpPwmPercent.put(o, Math.round(tmpMaxPwm / MAX_PWM));
    }

    public void removeMaxForUser(final Object o) {
        tmpPwmMax.remove(o);
        tmpPwmPercent.remove(o);
    }

    public int getPwm() {
        return currentPwm;
    }

}
