package handler;

import util.PinMappings;
import util.PulseData;

/**
 * Created by hendrik on 19.07.2017.
 * Pulse: turn power up and down in a wave pattern
 */
public class PulseHandler extends SequenceHandler {

    private final PulseData pulse;
    private final boolean transition;
    private final PinOutHandler pinHandler;

    public PulseHandler(final PulseData pulse, final boolean transition) {
        super(pulse.pin,pulse.forceClaim);
        this.pulse = pulse;
        this.transition = transition;
        pinHandler = PinMappings.getInstance().getHandler(pulse.pin);
        if (pulse.maxPwm != null) {
            pinHandler.setMaxForUserPercent(pulse.maxPwm, this);
        }
    }

    public void run() {
        registerThread();
        if (pulse.forceClaim) {
            pinHandler.forceClaim(this);
        }
        int currentPwm;
        if (transition) {
            currentPwm = pinHandler.getPwm();
        } else {
            currentPwm = pulse.lowerBound.getNumber();
        }
        int target = pulse.upperBound.getNumber();
        boolean up = false;
        while (!checkForStop()) {
            currentPwm = progressToTarget(currentPwm, target);
            if (up) {
                target = pulse.upperBound.getNumber();
            } else {
                target = pulse.lowerBound.getNumber();
            }
            up = !up;
            doStopConditionStuff();
        }
        if (setZero) {
            pinHandler.setOff(this);
        }
        pinHandler.release(this);
        unregisterThread();
    }

    protected boolean checkForStop() {
        return stop;
    }

    protected void doStopConditionStuff() {
        return;
    }

    private int progressToTarget(int current, final int target) {
        if (current == target) {
            return current;
        }
        final int stepPositive = pulse.stepSize.getNumber();
        final int step = stepPositive * (current < target ? 1 : -1);
        final int runtime = Math.abs(current - target);
        int runtimeCounter = 0;
        final int delay = pulse.stepDelay.getNumber();
        while (runtimeCounter < runtime && !checkForStop()) {
            runtimeCounter += stepPositive;
            current = current + step;
            pinHandler.setPercent(current, this);
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return current;
    }

}
