package handler;

import util.PulseData;

/**
 * Created by hendrik on 19.07.2017.
 */
public class SynchronMultiPinBuildUpHandler extends SynchronMultiPinPulseHandler implements SelfEndingHandler {
    public SynchronMultiPinBuildUpHandler(final PulseData pulse, final boolean transition) {
        super(pulse, transition);
    }

    @Override
    protected void doStopConditionStuff() {
        stop = true;
    }

    public void h_start() {
        this.start();
    }

    public void h_stop(boolean setZero) {
        this.stop(setZero);
    }

    public void h_join() throws InterruptedException {
        this.join();
    }

    public void h_join(long miliseconds) throws InterruptedException {
        this.join(miliseconds);
    }
}
