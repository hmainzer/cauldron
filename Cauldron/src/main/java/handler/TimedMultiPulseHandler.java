package handler;

import util.PulseData;

/**
 * Created by hendrik on 21.07.2017.
 */
public class TimedMultiPulseHandler extends MultiPulseHandler implements SelfEndingHandler {
    private final long runtime;
    private boolean firstCheck = true;

    public TimedMultiPulseHandler(boolean transition, String pin_handle, final long runtime, PulseData... pulses) {
        super(transition, pin_handle, pulses);
        this.runtime = runtime;
    }

    @Override
    protected boolean checkForStop() {
        if (firstCheck) {
            firstCheck = false;
            final Runnable r = new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(runtime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    stopSignalCallback();
                }
            };
            (new Thread(r)).start();
        }
        return stop;
    }

    private void stopSignalCallback() {
        this.stop(false);
    }

    @Override
    protected void doStopConditionStuff() {
        return;
    }

    public void h_start() {
        this.start();
    }

    public void h_stop(boolean setZero) {
        this.stop(setZero);
    }

    public void h_join() throws InterruptedException {
        this.join();
    }

    public void h_join(long miliseconds) throws InterruptedException {
        this.join(miliseconds);
    }
}
