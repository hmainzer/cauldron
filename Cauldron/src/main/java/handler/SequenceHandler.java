package handler;

import util.SequenceManager;

/**
 * Created by hendrik on 19.07.2017.
 */
public abstract class SequenceHandler extends Thread {
    protected boolean stop = false;
    protected boolean setZero = false;
    private boolean forceClaim = false;
    private String pin_handle;

    public SequenceHandler(String pin_handle, boolean forceClaim) {
        this.pin_handle = pin_handle;
        this.forceClaim = forceClaim;
    }

    public void stop(final boolean setZero) {
        this.stop = true;
        this.setZero = setZero;
    }

    public boolean isForceClaim() {
        return forceClaim;
    }

    public String getPinHandle() {
        return pin_handle;
    }

    protected void registerThread() {
        SequenceManager.getInstance().registerThread(this);
    }

    protected void unregisterThread() {
        SequenceManager.getInstance().unregisterThread(this);
    }

}