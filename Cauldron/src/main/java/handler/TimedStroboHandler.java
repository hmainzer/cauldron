package handler;

import util.StroboData;

/**
 * Created by hendrik on 19.07.2017.
 */
public class TimedStroboHandler extends StroboHandler implements SelfEndingHandler {
    private final long runtime;
    private boolean firstCheck = true;

    public TimedStroboHandler(final StroboData strobo, final long runtime) {
        super(strobo);
        this.runtime = runtime;
    }

    @Override
    protected boolean checkForStop() {
        if (firstCheck) {
            firstCheck = false;
            final Runnable r = new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(runtime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    stopSignalCallback();
                }
            };
            (new Thread(r)).start();
        }
        return stop;
    }

    private void stopSignalCallback() {
        this.stop(false);
    }

    public void h_start() {
        this.start();
    }

    public void h_stop(boolean setZero) {
        this.stop(setZero);
    }

    public void h_join() throws InterruptedException {
        this.join();
    }

    public void h_join(long miliseconds) throws InterruptedException {
        this.join(miliseconds);
    }
}
