package handler;

import util.PulseData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hendrik on 19.07.2017.
 */
public class MultiPulseHandler extends SequenceHandler {
    private final List<SequenceHandler> pulseHandlers;

    public MultiPulseHandler(final boolean transition, final String pin_handle, final PulseData... pulses) {
        super(pin_handle, pulses.length > 0 ? pulses[0].forceClaim : false);
        pulseHandlers = new ArrayList<SequenceHandler>();
        for (PulseData pd : pulses) {
            if (!pd.isMultiple) {
                pulseHandlers.add(new PulseHandler(pd, transition));
            } else {
                pulseHandlers.add(new SynchronMultiPinPulseHandler(pd, transition));
            }
        }
    }

    @Override
    public void run() {
        registerThread();
        for (SequenceHandler ph : pulseHandlers) {
            ph.start();
            System.out.println("start");
        }
        while (!checkForStop()) {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("stopped");
        for (SequenceHandler ph : pulseHandlers) {
            ph.stop(setZero);
        }
        for (SequenceHandler ph : pulseHandlers) {
            try {
                ph.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        unregisterThread();
    }


    protected boolean checkForStop() {
        return stop;
    }

    protected void doStopConditionStuff() {
        return;
    }
}
