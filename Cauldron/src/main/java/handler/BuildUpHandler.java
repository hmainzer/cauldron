package handler;

import util.PulseData;

/**
 * Created by hendrik on 19.07.2017.
 * only executes a pulse from low bound to upper bound once, then stops by itself -> stair pattern, best used with transition after that
 */
public class BuildUpHandler extends PulseHandler implements SelfEndingHandler {

    public BuildUpHandler(final PulseData pulse, final boolean transition) {
        super(pulse, transition);
    }

    @Override
    protected void doStopConditionStuff() {
        stop = true; // run only once to UppberBound
    }

    public void h_start() {
        this.start();
    }

    public void h_stop(boolean setZero) {
        this.stop(setZero);
    }

    public void h_join() throws InterruptedException {
        this.join();
    }

    public void h_join(long miliseconds) throws InterruptedException {
        this.join(miliseconds);
    }
}
