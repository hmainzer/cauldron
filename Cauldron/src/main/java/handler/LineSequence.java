package handler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hendrik on 19.07.2017.
 * Some Handlers executed after each other, only handlers with an build in end condition
 */
public class LineSequence extends SequenceHandler {
    private final List<SelfEndingHandler> handlers;
    private SelfEndingHandler currentlyRunningHandler;

    public LineSequence(String pin_handle, boolean force) {
        super(pin_handle, force);
        handlers = new ArrayList<SelfEndingHandler>();
    }

    public LineSequence addHandler(SelfEndingHandler handler) {
        handlers.add(handler);
        return this;
    }

    @Override
    public void run() {
        registerThread();
        currentlyRunningHandler = handlers.get(0);
        currentlyRunningHandler.h_start();
        for (int i = 1; i < handlers.size() && !stop; i++) {
            try {
                currentlyRunningHandler.h_join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            currentlyRunningHandler = handlers.get(i);
            currentlyRunningHandler.h_start();
        }
        unregisterThread();
    }

    @Override
    public void stop(boolean setZero) {
        super.stop(setZero);
        if (currentlyRunningHandler != null) {
            currentlyRunningHandler.h_stop(setZero);
        }
    }
}
