package handler;

/**
 * Created by hendrik on 19.07.2017.
 * Markup interface for Handlers with an build in ending condition. Delegate Functions to corresponding functions of Thread and SequenceHandler
 */
public interface SelfEndingHandler {
    public void h_start();
    public void h_stop(final boolean setZero);
    public void h_join() throws InterruptedException;
    public void h_join(long miliseconds) throws InterruptedException;
}
