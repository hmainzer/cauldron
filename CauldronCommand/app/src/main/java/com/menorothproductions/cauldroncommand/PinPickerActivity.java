package com.menorothproductions.cauldroncommand;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ActionMenuItemView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class PinPickerActivity extends AppCompatActivity {

    private TextView nrRed, nrBlue, nrGreen, nrWhite, nrPump1, nrPump2, nrExt, nrRedF, nrBlueF, nrGreenF;
    private SeekBar seekRed, seekBlue, seekGreen, seekWhite, seekPump1, seekPump2, seekExt, seekRedF, seekBlueF, seekGreenF;
    private Switch switchRed, switchBlue, switchGreen, switchWhite, switchPump1, switchPump2, switchExt, switchRedF, switchBlueF, switchGreenF;
    private boolean redActive, greenActive, blueActive, whiteActive, pump1Active, pump2Active, extActive, redfActive, greenfActive, bluefActive;
    private int redPwm, greenPwm, bluePwm, whitePwm, pump1Pwm, pump2Pwm, extPwm, redfPwm, greenfPwm, bluefPwm;
    private Button connectButton, debugButton;
    private EditText serverAddressEdit;
    private String serverAddress;
    private ClientThread socketThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serverAddressEdit = (EditText) findViewById(R.id.edit_SocketIP);
        connectButton = (Button) findViewById(R.id.btn_connect);
        debugButton = (Button) findViewById(R.id.btn_debug);
        socketThread = new ClientThread();

        nrRed = (TextView) findViewById(R.id.txt_red_nr);
        nrGreen = (TextView) findViewById(R.id.txt_green_nr);
        nrBlue = (TextView) findViewById(R.id.txt_blue_nr);
        nrWhite = (TextView) findViewById(R.id.txt_white_nr);
        nrPump1 = (TextView) findViewById(R.id.txt_pump1_nr);
        nrPump2 = (TextView) findViewById(R.id.txt_pump2_nr);
        nrExt = (TextView) findViewById(R.id.txt_ext_nr);
        nrRedF = (TextView) findViewById(R.id.txt_redf_nr);
        nrGreenF = (TextView) findViewById(R.id.txt_greenf_nr);
        nrBlueF = (TextView) findViewById(R.id.txt_bluef_nr);

        seekRed = (SeekBar) findViewById(R.id.seekRed);
        seekGreen = (SeekBar) findViewById(R.id.seekGreen);
        seekBlue = (SeekBar) findViewById(R.id.seekBlue);
        seekWhite = (SeekBar) findViewById(R.id.seekWhite);
        seekPump1 = (SeekBar) findViewById(R.id.seekPump1);
        seekPump2 = (SeekBar) findViewById(R.id.seekPump2);
        seekExt = (SeekBar) findViewById(R.id.seekExt);
        seekRedF = (SeekBar) findViewById(R.id.seekRedF);
        seekGreenF = (SeekBar) findViewById(R.id.seekGreenF);
        seekBlueF = (SeekBar) findViewById(R.id.seekBlueF);

        switchRed = (Switch) findViewById(R.id.swtRed);
        switchGreen = (Switch) findViewById(R.id.swtGreen);
        switchBlue = (Switch) findViewById(R.id.swtBlue);
        switchWhite = (Switch) findViewById(R.id.swtWhite);
        switchPump1 = (Switch) findViewById(R.id.swtPump1);
        switchPump2 = (Switch) findViewById(R.id.swtPump2);
        switchExt = (Switch) findViewById(R.id.swtExt);
        switchRedF = (Switch) findViewById(R.id.swtRedF);
        switchGreenF = (Switch) findViewById(R.id.swtGreenF);
        switchBlueF = (Switch) findViewById(R.id.swtBlueF);

        redActive = false;
        greenActive = false;
        blueActive = false;
        whiteActive = false;
        pump1Active = false;
        pump2Active = false;
        extActive = false;
        redfActive = false;
        greenfActive = false;
        bluefActive = false;

        redPwm = 100;
        greenPwm = 100;
        bluePwm = 100;
        whitePwm = 100;
        pump1Pwm = 100;
        pump2Pwm = 100;
        extPwm = 100;
        redfPwm = 100;
        greenfPwm = 100;
        bluefPwm = 100;

        seekRed.setProgress(100);
        seekGreen.setProgress(100);
        seekBlue.setProgress(100);
        seekWhite.setProgress(100);
        seekPump1.setProgress(100);
        seekPump2.setProgress(100);
        seekExt.setProgress(100);
        seekRedF.setProgress(100);
        seekGreenF.setProgress(100);
        seekBlueF.setProgress(100);

        switchRed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                redActive = b;
                setLED("C_RED", redPwm, redActive);
            }
        });

        seekRed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                nrRed.setText(Integer.toString(i));
                redPwm = i;
                setLED("C_RED", redPwm, redActive);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switchGreen.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                greenActive = b;
                setLED("C_GREEN", greenPwm, greenActive);
            }
        });

        seekGreen.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                nrGreen.setText(Integer.toString(i));
                greenPwm = i;
                setLED("C_GREEN", greenPwm, greenActive);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switchBlue.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                blueActive = b;
                setLED("C_BLUE", bluePwm, blueActive);
            }
        });

        seekBlue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                nrBlue.setText(Integer.toString(i));
                bluePwm = i;
                setLED("C_BLUE", bluePwm, blueActive);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switchWhite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                whiteActive = b;
                setLED("C_WHITE", whitePwm, whiteActive);
            }
        });

        seekWhite.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                nrWhite.setText(Integer.toString(i));
                whitePwm = i;
                setLED("C_WHITE", whitePwm, whiteActive);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switchPump1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                pump1Active = b;
                setLED("C_PUMP1", pump1Pwm, pump1Active);
            }
        });

        seekPump1.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                nrPump1.setText(Integer.toString(i));
                pump1Pwm = i;
                setLED("C_PUMP1", pump1Pwm, pump1Active);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switchPump2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                pump2Active = b;
                setLED("C_PUMP2", pump2Pwm, pump2Active);
            }
        });

        seekPump2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                nrPump2.setText(Integer.toString(i));
                pump2Pwm = i;
                setLED("C_PUMP2", pump2Pwm, pump2Active);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switchExt.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                extActive = b;
                setLED("C_EXT", extPwm, extActive);
            }
        });

        seekExt.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                nrExt.setText(Integer.toString(i));
                extPwm = i;
                setLED("C_EXT", extPwm, extActive);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switchRedF.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                redfActive = b;
                setLED("F_RED", redfPwm, redfActive);
            }
        });

        seekRedF.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                nrRedF.setText(Integer.toString(i));
                redfPwm = i;
                setLED("F_RED", redfPwm, redfActive);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switchGreenF.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                greenfActive = b;
                setLED("F_GREEN", greenfPwm, greenfActive);
            }
        });

        seekGreenF.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                nrGreenF.setText(Integer.toString(i));
                greenfPwm = i;
                setLED("F_GREEN", greenfPwm, greenfActive);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switchBlueF.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                bluefActive = b;
                setLED("F_BLUE", bluefPwm, bluefActive);
            }
        });

        seekBlueF.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                nrBlueF.setText(Integer.toString(i));
                bluefPwm = i;
                setLED("F_BLUE", bluefPwm, bluefActive);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!socketThread.isConnected()) {
                    serverAddress = serverAddressEdit.getText().toString();
                    if (!serverAddress.equals("")) {
                        Thread t = new Thread(socketThread);
                        t.start();
                    } else {
                        Log.d("!!!", "No IP!");
                    }
                } else {
                    socketThread.disconnect();
                }
            }
        });

        debugButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setLED("DEBUG", 0, true);
            }
        });
    }

    private void setLED(String target, int pwm, boolean active) {
        //Toast toast = Toast.makeText(getApplicationContext(), target + " : " + (active ? "on" : "off") + " : pwm:" + pwm, Toast.LENGTH_SHORT);
        //toast.show();
        Log.d("ClientActivity", "Target: " + target + "  :   PWM: " + pwm + "    :  active: " + active);
        if (socketThread.isConnected()) {
            if (!active) {
                socketThread.sendCommand("SET#" + target + ";OFF");
            }
            if (active) {
                socketThread.sendCommand("SET#" + target + ";PWM:" + pwm);
            }
        }
    }

    public class ClientThread implements Runnable {

        private Socket socket;
        private boolean connected;

        public void disconnect() {
            connected = false;
        }

        public boolean isConnected() {
            return connected;
        }

        public ClientThread() {
            connected = false;
        }

        public void run() {
            try {
                Log.d("ClientActivity", "C: Connecting...");
                socket = new Socket(serverAddress, 12345);
                connected = true;
                //  connectButton.setText("Disconnect");
                while (connected) {
                    Log.d("ClientActivity", "C: Wait.");
                    Thread.sleep(3000);
                }
                socket.close();
                Log.d("ClientActivity", "C: Closed.");
            } catch (Exception e) {
                Log.e("ClientActivity", "C: Error", e);
                connected = false;
            }
        }

        public boolean sendCommand(String command) {
            if (connected) {
                try {
                    Log.d("ClientActivity", "C: Sending command: " + command);
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket
                            .getOutputStream())), true);
                    out.println(command);
                    Log.d("ClientActivity", "C: Sent.");
                    return true;
                } catch (Exception e) {
                    Log.e("ClientActivity", "S: Error", e);
                    connected = false;
                }
            }
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        if (socketThread!= null){
            socketThread.disconnect();
        }
        Intent i = new Intent(getApplicationContext(), NavigationActivity.class);
        startActivity(i);
        finish();
    }
}
