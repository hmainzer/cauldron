package com.menorothproductions.cauldroncommand;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

public class CreatePulseActivity extends AppCompatActivity {

    private Button btnSave, btnLoad, btnDelete, btnAdd, btnRemove, btnSet, btnNew;
    private EditText editName, editTime, editPwmMax, editLowBase, editHighBase, editStepBase, editDelayBase, editLowDice, editHighDice, editStepDice, editDelayDice;
    private Switch swtForce, swtTransit, swtZero, swtTime, swtLowInv, swtHighInv, swtStepInv, swtDelayInv;
    private Spinner spinMainHdl, spinPwmMaxHdl, spinPulseHdl;
    private Map<String, Integer> pwmMaxMap = new HashMap<>();
    private Map<String, PulseValueSet> pulseValueMap = new HashMap<>();

    public CreatePulseActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_pulse);

        pin_handles_tree = new HashMap<String, String[]>();
        pin_handles_tree.put("ALL", ALL);
        pin_handles_tree.put("CAULDRON", CAULDRON);
        pin_handles_tree.put("C_LIGHT", C_LIGHT);
        pin_handles_tree.put("C_RED", C_RED);
        pin_handles_tree.put("C_BLUE", C_BLUE);
        pin_handles_tree.put("C_GREEN", C_GREEN);
        pin_handles_tree.put("C_WHITE", C_WHITE);
        pin_handles_tree.put("C_UTIL", C_UTIL);
        pin_handles_tree.put("C_PUMPS", C_PUMPS);
        pin_handles_tree.put("C_PUMP1", C_PUMP1);
        pin_handles_tree.put("C_PUMP2", C_PUMP2);
        pin_handles_tree.put("C_EXT", C_EXT);
        pin_handles_tree.put("FIRE", FIRE);
        pin_handles_tree.put("F_RED", F_RED);
        pin_handles_tree.put("F_BLUE", F_BLUE);
        pin_handles_tree.put("F_GREEN", F_GREEN);

        pin_handles = new HashMap<String, String[]>();
        pin_handles.put("ALL", ALL_PINS);
        pin_handles.put("CAULDRON", CAULDRON_PINS);
        pin_handles.put("C_LIGHT", C_LIGHT);
        pin_handles.put("C_RED", C_RED);
        pin_handles.put("C_BLUE", C_BLUE);
        pin_handles.put("C_GREEN", C_GREEN);
        pin_handles.put("C_WHITE", C_WHITE);
        pin_handles.put("C_UTIL", C_UTIL_PINS);
        pin_handles.put("C_PUMPS", C_PUMPS);
        pin_handles.put("C_PUMP1", C_PUMP1);
        pin_handles.put("C_PUMP2", C_PUMP2);
        pin_handles.put("C_EXT", C_EXT);
        pin_handles.put("FIRE", FIRE);
        pin_handles.put("F_RED", F_RED);
        pin_handles.put("F_BLUE", F_BLUE);
        pin_handles.put("F_GREEN", F_GREEN);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnLoad = (Button) findViewById(R.id.btnLoad);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnAdd = (Button) findViewById(R.id.btnAddPulse);
        btnRemove = (Button) findViewById(R.id.btnRemovePulse);
        btnSet = (Button) findViewById(R.id.btnPwmMaxSet);
        btnNew = (Button) findViewById(R.id.btnNew);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ControlActivity.setDefaults(editName.getText().toString(), generateCommandStringFromCurrentState(), getActivity());
            }
        });

        btnLoad.setEnabled(false);
        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String command = ControlActivity.getDefaults(editName.getText().toString(), getActivity());
                if (command != null) {
                    loadFromCommand(command);
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ControlActivity.removeDefaults(editName.getText().toString(), getActivity());
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PulseValueSet pvs = new PulseValueSet(buildValueString(editLowBase.getText().toString(), editLowDice.getText().toString()), buildValueString(editHighBase.getText().toString(), editHighDice.getText().toString()), buildValueString(editStepBase.getText().toString(), editStepDice.getText().toString()), buildValueString(editDelayBase.getText().toString(), editDelayDice.getText().toString()), swtLowInv.isChecked(), swtHighInv.isChecked(), swtStepInv.isChecked(), swtDelayInv.isChecked());
                String selection = spinPulseHdl.getSelectedItem().toString();
                if (selection.contains(":")) {
                    selection = selection.substring(0, selection.indexOf(":")).trim();
                }
                pulseValueMap.put(selection, pvs);
                setPulseSpinner(spinMainHdl.getSelectedItem().toString());
            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selection = spinPulseHdl.getSelectedItem().toString();
                if (selection.contains(":")) {
                    selection = selection.substring(0, selection.indexOf(":")).trim();
                }
                pulseValueMap.remove(selection);
                setPulseSpinner(spinMainHdl.getSelectedItem().toString());
            }
        });

        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selection = spinPwmMaxHdl.getSelectedItem().toString();
                if (selection.contains(":")) {
                    selection = selection.substring(0, selection.indexOf(":")).trim();
                }
                int value = Integer.parseInt(editPwmMax.getText().toString());
                if (value >= 100 || value < 0) {
                    pwmMaxMap.remove(selection);
                } else {
                    pwmMaxMap.put(selection, value);
                }
                setPwmSpinner(spinMainHdl.getSelectedItem().toString());
            }
        });

        btnNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAll();
            }
        });

        editName = (EditText) findViewById(R.id.editPulseName);
        editTime = (EditText) findViewById(R.id.editTimer);
        editPwmMax = (EditText) findViewById(R.id.editPwmMax);
        editLowBase = (EditText) findViewById(R.id.editLowBase);
        editHighBase = (EditText) findViewById(R.id.editHighBase);
        editStepBase = (EditText) findViewById(R.id.editStepBase);
        editDelayBase = (EditText) findViewById(R.id.editDelayBase);
        editLowDice = (EditText) findViewById(R.id.editLowDice);
        editHighDice = (EditText) findViewById(R.id.editHighDice);
        editStepDice = (EditText) findViewById(R.id.editStepDice);
        editDelayDice = (EditText) findViewById(R.id.editDelayDice);

        swtForce = (Switch) findViewById(R.id.swtForce);
        swtTransit = (Switch) findViewById(R.id.swtTransit);
        swtZero = (Switch) findViewById(R.id.swtSetZero);
        swtTime = (Switch) findViewById(R.id.swtTimer);
        swtLowInv = (Switch) findViewById(R.id.swtLowInverse);
        swtHighInv = (Switch) findViewById(R.id.swtHighInverse);
        swtStepInv = (Switch) findViewById(R.id.swtStepInverse);
        swtDelayInv = (Switch) findViewById(R.id.swtDelayInverse);

        spinMainHdl = (Spinner) findViewById(R.id.mainHdlSpinner);
        spinPwmMaxHdl = (Spinner) findViewById(R.id.pwmMaxSpinner);
        spinPulseHdl = (Spinner) findViewById(R.id.pulseSpinner);

        spinMainHdl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println(adapterView.getSelectedItem().toString());
                setPulseSpinner(adapterView.getSelectedItem().toString());
                setPwmSpinner(adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                spinPwmMaxHdl.setAdapter(null);
                spinPulseHdl.setAdapter(null);
            }
        });

        spinPwmMaxHdl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selection = adapterView.getSelectedItem().toString();
                if (selection.contains(":")) {
                    String pwm = selection.substring(selection.indexOf(':') + 1).trim();
                    editPwmMax.setText(pwm);
                } else {
                    editPwmMax.setText("100");
                }
                btnSet.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                btnSet.setEnabled(false);
            }
        });

        spinPulseHdl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selection = adapterView.getSelectedItem().toString();
                if (selection.contains(":")) {
                    String pin = selection.substring(0, selection.indexOf(':')).trim();
                    PulseValueSet pvs = pulseValueMap.get(pin);
                    editLowBase.setText(getBaseFromValueString(pvs.low));
                    editHighBase.setText(getBaseFromValueString(pvs.high));
                    editStepBase.setText(getBaseFromValueString(pvs.step));
                    editDelayBase.setText(getBaseFromValueString(pvs.delay));
                    editLowDice.setText(getDiceFromValueStirng(pvs.low));
                    editHighDice.setText(getDiceFromValueStirng(pvs.high));
                    editStepDice.setText(getDiceFromValueStirng(pvs.step));
                    editDelayDice.setText(getDiceFromValueStirng(pvs.delay));
                    swtLowInv.setChecked(pvs.lowInv);
                    swtHighInv.setChecked(pvs.highInv);
                    swtStepInv.setChecked(pvs.stepInv);
                    swtDelayInv.setChecked(pvs.delayInv);

                } else {
                    editLowBase.setText("");
                    editHighBase.setText("");
                    editStepBase.setText("");
                    editDelayBase.setText("");
                    editLowDice.setText("");
                    editHighDice.setText("");
                    editStepDice.setText("");
                    editDelayDice.setText("");
                    swtLowInv.setChecked(false);
                    swtHighInv.setChecked(false);
                    swtStepInv.setChecked(false);
                    swtDelayInv.setChecked(false);
                }
                btnAdd.setEnabled(true);
                btnRemove.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                btnAdd.setEnabled(false);
                btnRemove.setEnabled(false);
            }
        });
        clearAll();
    }

    private void loadFromCommand(String command) {
    }

    private void clearAll() {
        pwmMaxMap.clear();
        pulseValueMap.clear();
        editName.setText("");
        editTime.setText("");
        editPwmMax.setText("");
        editLowBase.setText("");
        editHighBase.setText("");
        editStepBase.setText("");
        editDelayBase.setText("");
        editLowDice.setText("");
        editHighDice.setText("");
        editStepDice.setText("");
        editDelayDice.setText("");

        swtForce.setChecked(false);
        swtTransit.setChecked(false);
        swtZero.setChecked(false);
        swtTime.setChecked(false);
        swtLowInv.setChecked(false);
        swtHighInv.setChecked(false);
        swtStepInv.setChecked(false);
        swtDelayInv.setChecked(false);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ALLALL); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinMainHdl.setAdapter(spinnerArrayAdapter);
    }

    private List<String> pwmSpinnerValues;
    private List<String> pulseSpinnerValues;

    public void setPwmSpinner(String mainHdl) {
        System.out.println(mainHdl);
        System.out.println(pin_handles_tree.get(mainHdl));
        pwmSpinnerValues = new ArrayList<String>();
        for (String s : pin_handles.get(mainHdl)) {
            if (pwmMaxMap.containsKey(s)) {
                pwmSpinnerValues.add(s + " : " + pwmMaxMap.get(s));
            } else {
                pwmSpinnerValues.add(s);
            }
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pwmSpinnerValues); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPwmMaxHdl.setAdapter(spinnerArrayAdapter);
    }

    public void setPulseSpinner(String mainHdl) {
        System.out.println(mainHdl);
        System.out.println(pin_handles_tree.get(mainHdl));
        pulseSpinnerValues = new ArrayList<String>();
        pulseSpinnerValues.add(mainHdl);
        for (String s : pin_handles_tree.get(mainHdl)) {
            if (pulseValueMap.containsKey(s)) {
                pulseSpinnerValues.add(s + " : in use");
            } else {
                pulseSpinnerValues.add(s);
            }
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pulseSpinnerValues); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPulseHdl.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), NavigationActivity.class);
        startActivity(i);
        finish();
    }

    private String buildValueString(String baseValue, String dice) {
        if (baseValue.isEmpty()) {
            baseValue = "0";
        }
        if (dice.isEmpty()) {
            return "VAL(" + baseValue + ")";
        }
        dice = dice.replace(" ", "");
        dice = dice.replace(",", ".");
        return "RND(" + baseValue + "." + dice + ")";
    }

    private String getBaseFromValueString(String valueString) {
        if (valueString.contains("VAL(")) {
            return valueString.substring(4, valueString.indexOf(')'));
        }
        return valueString.substring(4, valueString.indexOf('.'));
    }

    private String getDiceFromValueStirng(String valueString) {
        if (valueString.contains("VAL(")) {
            return "";
        }
        return valueString.substring(valueString.indexOf('.') + 1, valueString.indexOf(')'));
    }

    private String generateCommandStringFromCurrentState() {
        String command = "PULSE#" + spinMainHdl.getSelectedItem().toString();
        String pulsedata = "";
        for (Map.Entry<String, PulseValueSet> e : pulseValueMap.entrySet()) {
            pulsedata = ";PULSEDATA#" + e.getKey() + "(";
            pulsedata = pulsedata + e.getValue().low + (e.getValue().lowInv ? "(INVERSE)" : "") + ",";
            pulsedata = pulsedata + e.getValue().high + (e.getValue().highInv ? "(INVERSE)" : "") + ",";
            pulsedata = pulsedata + e.getValue().step + (e.getValue().stepInv ? "(INVERSE)" : "") + ",";
            pulsedata = pulsedata + e.getValue().delay + (e.getValue().delayInv ? "(INVERSE)" : "");
            pulsedata = pulsedata + ")";
            command = command + pulsedata;
        }
        String pwmMax;
        for (Map.Entry<String, Integer> e : pwmMaxMap.entrySet()) {
            pwmMax = ";MAX_PWM#" + e.getKey() + ":" + e.getValue();
            command = command + pwmMax;
        }
        if (swtForce.isChecked()) {
            command = command + ";DO_FORCE";
        }
        if (swtTransit.isChecked()) {
            command = command + ";DO_TRANSIT";
        }
        if (swtZero.isChecked()) {
            command = command + ";DO_SETZERO";
        }
        if (swtTime.isChecked()) {
            command = command + ";TIMER(" + editTime.getText().toString() + ")";
        }
        System.out.println(command);
        return command;
    }

    private Map<String, String[]> pin_handles_tree;
    private Map<String, String[]> pin_handles;
    public final static String[] ALLALL = {"ALL", "CAULDRON", "C_LIGHT", "C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_UTIL", "C_PUMPS", "C_PUMP1", "C_PUMP2", "C_EXT", "FIRE", "F_RED", "F_GREEN", "F_BLUE"};
    public final static String[] ALL = {"CAULDRON", "C_LIGHT", "C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_UTIL", "C_PUMPS", "C_PUMP1", "C_PUMP2", "C_EXT", "FIRE", "F_RED", "F_GREEN", "F_BLUE"};
    public final static String[] ALL_PINS = {"C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_PUMP1", "C_PUMP2", "C_EXT", "F_RED", "F_GREEN", "F_BLUE"};
    public final static String[] CAULDRON = {"C_LIGHT", "C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_UTIL", "C_PUMPS", "C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] CAULDRON_PINS = {"C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] C_LIGHT = {"C_RED", "C_BLUE", "C_GREEN", "C_WHITE"};
    public final static String[] C_RED = {"C_RED"};
    public final static String[] C_BLUE = {"C_BLUE"};
    public final static String[] C_GREEN = {"C_GREEN"};
    public final static String[] C_WHITE = {"C_WHITE"};
    public final static String[] C_UTIL = {"C_PUMPS", "C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] C_UTIL_PINS = {"C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] C_PUMPS = {"C_PUMP1", "C_PUMP2"};
    public final static String[] C_PUMP1 = {"C_PUMP1"};
    public final static String[] C_PUMP2 = {"C_PUMP2"};
    public final static String[] C_EXT = {"C_EXT"};
    public final static String[] FIRE = {"F_RED", "F_GREEN", "F_BLUE"};
    public final static String[] F_RED = {"F_RED"};
    public final static String[] F_BLUE = {"F_GREEN"};
    public final static String[] F_GREEN = {"F_BLUE"};

    public Context getActivity() {
        return this;
    }

    private class PulseValueSet {
        public String low, high, step, delay;
        public boolean lowInv, highInv, stepInv, delayInv;

        public PulseValueSet(String low, String high, String step, String delay, boolean lowInv, boolean highInv, boolean stepInv, boolean delayInv) {
            this.low = low;
            this.high = high;
            this.step = step;
            this.delay = delay;
            this.lowInv = lowInv;
            this.highInv = highInv;
            this.stepInv = stepInv;
            this.delayInv = delayInv;
        }
    }
}
