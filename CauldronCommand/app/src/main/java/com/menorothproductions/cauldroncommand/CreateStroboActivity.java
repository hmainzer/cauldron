package com.menorothproductions.cauldroncommand;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateStroboActivity extends AppCompatActivity {

    private Button btnSave, btnLoad, btnDelete, btnAdd, btnRemove, btnSet, btnNew;
    private EditText editName, editTime, editPwmMax, editFlashBase, editHighBase, editDelayBase, editFlashDice, editHighDice, editDelayDice;
    private Switch swtForce, swtTransit, swtZero, swtTime, swtFlashInv, swtHighInv, swtDelayInv;
    private Spinner spinMainHdl, spinPwmMaxHdl, spinStrobeHdl;
    private Map<String, Integer> pwmMaxMap = new HashMap<>();
    private Map<String, StrobeValueSet> strobeValueMap = new HashMap<>();
    private Switch swtLowZero;

    public CreateStroboActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_strobo);

        pin_handles_tree = new HashMap<String, String[]>();
        pin_handles_tree.put("ALL", ALL);
        pin_handles_tree.put("CAULDRON", CAULDRON);
        pin_handles_tree.put("C_LIGHT", C_LIGHT);
        pin_handles_tree.put("C_RED", C_RED);
        pin_handles_tree.put("C_BLUE", C_BLUE);
        pin_handles_tree.put("C_GREEN", C_GREEN);
        pin_handles_tree.put("C_WHITE", C_WHITE);
        pin_handles_tree.put("C_UTIL", C_UTIL);
        pin_handles_tree.put("C_PUMPS", C_PUMPS);
        pin_handles_tree.put("C_PUMP1", C_PUMP1);
        pin_handles_tree.put("C_PUMP2", C_PUMP2);
        pin_handles_tree.put("C_EXT", C_EXT);
        pin_handles_tree.put("FIRE", FIRE);
        pin_handles_tree.put("F_RED", F_RED);
        pin_handles_tree.put("F_BLUE", F_BLUE);
        pin_handles_tree.put("F_GREEN", F_GREEN);

        pin_handles = new HashMap<String, String[]>();
        pin_handles.put("ALL", ALL_PINS);
        pin_handles.put("CAULDRON", CAULDRON_PINS);
        pin_handles.put("C_LIGHT", C_LIGHT);
        pin_handles.put("C_RED", C_RED);
        pin_handles.put("C_BLUE", C_BLUE);
        pin_handles.put("C_GREEN", C_GREEN);
        pin_handles.put("C_WHITE", C_WHITE);
        pin_handles.put("C_UTIL", C_UTIL_PINS);
        pin_handles.put("C_PUMPS", C_PUMPS);
        pin_handles.put("C_PUMP1", C_PUMP1);
        pin_handles.put("C_PUMP2", C_PUMP2);
        pin_handles.put("C_EXT", C_EXT);
        pin_handles.put("FIRE", FIRE);
        pin_handles.put("F_RED", F_RED);
        pin_handles.put("F_BLUE", F_BLUE);
        pin_handles.put("F_GREEN", F_GREEN);

        btnSave = (Button) findViewById(R.id.btnSaveSt);
        btnLoad = (Button) findViewById(R.id.btnLoadSt);
        btnDelete = (Button) findViewById(R.id.btnDeleteSt);
        btnSet = (Button) findViewById(R.id.btnPwmMaxSetSt);
        btnNew = (Button) findViewById(R.id.btnNewSt);
        btnAdd = (Button) findViewById(R.id.btnAddStrobeSt);
        btnRemove = (Button) findViewById(R.id.btnRemoveStrobeSt);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ControlActivity.setDefaults(editName.getText().toString(), generateCommandStringFromCurrentState(), getActivity());
            }
        });

        btnLoad.setEnabled(false);
        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String command = ControlActivity.getDefaults(editName.getText().toString(), getActivity());
                if (command != null) {
                    loadFromCommand(command);
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ControlActivity.removeDefaults(editName.getText().toString(), getActivity());
            }
        });

        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selection = spinPwmMaxHdl.getSelectedItem().toString();
                if (selection.contains(":")) {
                    selection = selection.substring(0, selection.indexOf(":")).trim();
                }
                int value = Integer.parseInt(editPwmMax.getText().toString());
                if (value >= 100 || value < 0) {
                    pwmMaxMap.remove(selection);
                } else {
                    pwmMaxMap.put(selection, value);
                }
                setPwmSpinner(spinMainHdl.getSelectedItem().toString());
            }
        });

        btnNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearAll();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StrobeValueSet pvs = new StrobeValueSet(buildValueString(editHighBase.getText().toString(), editHighDice.getText().toString()), buildValueString(editFlashBase.getText().toString(), editFlashDice.getText().toString()), buildValueString(editDelayBase.getText().toString(), editDelayDice.getText().toString()), swtHighInv.isChecked(), swtFlashInv.isChecked(), swtDelayInv.isChecked(), swtLowZero.isChecked());
                String selection = spinStrobeHdl.getSelectedItem().toString();
                if (selection.contains(":")) {
                    selection = selection.substring(0, selection.indexOf(":")).trim();
                }
                strobeValueMap.put(selection, pvs);
                setStrobeSpinner(spinMainHdl.getSelectedItem().toString());
            }
        });

        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String selection = spinStrobeHdl.getSelectedItem().toString();
                if (selection.contains(":")) {
                    selection = selection.substring(0, selection.indexOf(":")).trim();
                }
                strobeValueMap.remove(selection);
                setStrobeSpinner(spinMainHdl.getSelectedItem().toString());
            }
        });

        editName = (EditText) findViewById(R.id.editStroboNameSt);
        editTime = (EditText) findViewById(R.id.editTimerSt);
        editPwmMax = (EditText) findViewById(R.id.editPwmMaxSt);
        editFlashBase = (EditText) findViewById(R.id.editFlashBaseSt);
        editHighBase = (EditText) findViewById(R.id.editHighBaseSt);
        editDelayBase = (EditText) findViewById(R.id.editDelayBaseSt);
        editFlashDice = (EditText) findViewById(R.id.editFlashDiceSt);
        editHighDice = (EditText) findViewById(R.id.editHighDiceSt);
        editDelayDice = (EditText) findViewById(R.id.editDelayDiceSt);

        swtForce = (Switch) findViewById(R.id.swtForceSt);
        swtTransit = (Switch) findViewById(R.id.swtTransitSt);
        swtZero = (Switch) findViewById(R.id.swtSetZeroSt);
        swtTime = (Switch) findViewById(R.id.swtTimerSt);
        swtFlashInv = (Switch) findViewById(R.id.swtFlashInverseSt);
        swtHighInv = (Switch) findViewById(R.id.swtHighInverseSt);
        swtLowZero = (Switch) findViewById(R.id.swtStLowZeroSt);
        swtDelayInv = (Switch) findViewById(R.id.swtDelayInverseSt);

        spinMainHdl = (Spinner) findViewById(R.id.mainHdlSpinnerSt);
        spinPwmMaxHdl = (Spinner) findViewById(R.id.pwmMaxSpinnerSt);
        spinStrobeHdl = (Spinner) findViewById(R.id.strobeSpinnerSt);

        spinMainHdl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                System.out.println(adapterView.getSelectedItem().toString());
                setPwmSpinner(adapterView.getSelectedItem().toString());
                setStrobeSpinner(adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                spinPwmMaxHdl.setAdapter(null);
            }
        });

        spinPwmMaxHdl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selection = adapterView.getSelectedItem().toString();
                if (selection.contains(":")) {
                    String pwm = selection.substring(selection.indexOf(':') + 1).trim();
                    editPwmMax.setText(pwm);
                } else {
                    editPwmMax.setText("100");
                }
                btnSet.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                btnSet.setEnabled(false);
            }
        });
        spinStrobeHdl.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String selection = adapterView.getSelectedItem().toString();
                if (selection.contains(":")) {
                    String pin = selection.substring(0, selection.indexOf(':')).trim();
                    StrobeValueSet pvs = strobeValueMap.get(pin);
                    editHighBase.setText(getBaseFromValueString(pvs.high));
                    editFlashBase.setText(getBaseFromValueString(pvs.flash));
                    editDelayBase.setText(getBaseFromValueString(pvs.delay));
                    editHighDice.setText(getDiceFromValueStirng(pvs.high));
                    editFlashDice.setText(getDiceFromValueStirng(pvs.flash));
                    editDelayDice.setText(getDiceFromValueStirng(pvs.delay));
                    swtLowZero.setChecked(pvs.lowZero);
                    swtHighInv.setChecked(pvs.highInv);
                    swtFlashInv.setChecked(pvs.flashInv);
                    swtDelayInv.setChecked(pvs.delayInv);

                } else {
                    editHighBase.setText("");
                    editFlashBase.setText("");
                    editDelayBase.setText("");
                    editHighDice.setText("");
                    editFlashDice.setText("");
                    editDelayDice.setText("");
                    swtLowZero.setChecked(false);
                    swtHighInv.setChecked(false);
                    swtFlashInv.setChecked(false);
                    swtDelayInv.setChecked(false);
                }
                btnAdd.setEnabled(true);
                btnRemove.setEnabled(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                btnAdd.setEnabled(false);
                btnRemove.setEnabled(false);
            }
        });
        clearAll();
    }

    private void loadFromCommand(String command) {
    }

    private void clearAll() {
        pwmMaxMap.clear();
        strobeValueMap.clear();
        editName.setText("");
        editTime.setText("");
        editPwmMax.setText("");
        editFlashBase.setText("");
        editHighBase.setText("");
        editDelayBase.setText("");
        editFlashDice.setText("");
        editHighDice.setText("");
        editDelayDice.setText("");

        swtForce.setChecked(false);
        swtTransit.setChecked(false);
        swtZero.setChecked(false);
        swtTime.setChecked(false);
        swtFlashInv.setChecked(false);
        swtHighInv.setChecked(false);
        swtDelayInv.setChecked(false);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, ALLALL); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinMainHdl.setAdapter(spinnerArrayAdapter);
    }

    private List<String> pwmSpinnerValues;
    private List<String> strobSpinnerValues;

    public void setPwmSpinner(String mainHdl) {
        System.out.println(mainHdl);
        System.out.println(pin_handles_tree.get(mainHdl));
        pwmSpinnerValues = new ArrayList<String>();
        for (String s : pin_handles.get(mainHdl)) {
            if (pwmMaxMap.containsKey(s)) {
                pwmSpinnerValues.add(s + " : " + pwmMaxMap.get(s));
            } else {
                pwmSpinnerValues.add(s);
            }
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pwmSpinnerValues); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinPwmMaxHdl.setAdapter(spinnerArrayAdapter);
    }

    public void setStrobeSpinner(String mainHdl) {
        System.out.println(mainHdl);
        System.out.println(pin_handles.get(mainHdl));
        strobSpinnerValues = new ArrayList<String>();
        strobSpinnerValues.add(mainHdl);
        for (String s : pin_handles.get(mainHdl)) {
            if (strobeValueMap.containsKey(s)) {
                strobSpinnerValues.add(s + " : in use");
            } else {
                strobSpinnerValues.add(s);
            }
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, strobSpinnerValues); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinStrobeHdl.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(getApplicationContext(), NavigationActivity.class);
        startActivity(i);
        finish();
    }

    private String buildValueString(String baseValue, String dice) {
        if (baseValue.isEmpty()) {
            baseValue = "0";
        }
        if (dice.isEmpty()) {
            return "VAL(" + baseValue + ")";
        }
        dice = dice.replace(" ", "");
        dice = dice.replace(",", ".");
        return "RND(" + baseValue + "." + dice + ")";
    }

    private String getBaseFromValueString(String valueString) {
        if (valueString.contains("VAL(")) {
            return valueString.substring(4, valueString.indexOf(')'));
        }
        return valueString.substring(4, valueString.indexOf('.'));
    }

    private String getDiceFromValueStirng(String valueString) {
        if (valueString.contains("VAL(")) {
            return "";
        }
        return valueString.substring(valueString.indexOf('.') + 1, valueString.indexOf(')'));
    }

    private String generateCommandStringFromCurrentState() {
        String command = "STROBE#" + spinMainHdl.getSelectedItem().toString();
       /* String strobedata = "STROBEDATA#" + spinMainHdl.getSelectedItem().toString() + "(";
        strobedata = strobedata + buildValueString(editHighBase.getText().toString(), editHighDice.getText().toString());
        strobedata = strobedata + "," + buildValueString(editFlashBase.getText().toString(), editFlashDice.getText().toString());
        strobedata = strobedata + "," + buildValueString(editDelayBase.getText().toString(), editDelayDice.getText().toString());
        strobedata = strobedata + (swtLowZero.isChecked() ? ",DO_LOWZERO" : "") + ")";
        command = command + strobedata;*/

        String strobedata = "";
        for (Map.Entry<String, StrobeValueSet> e : strobeValueMap.entrySet()) {
            strobedata = ";STROBEDATA#" + e.getKey() + "(";
            strobedata = strobedata + e.getValue().high + (e.getValue().highInv ? "(INVERSE)" : "") + ",";
            strobedata = strobedata + e.getValue().flash + (e.getValue().flashInv ? "(INVERSE)" : "") + ",";
            strobedata = strobedata + e.getValue().delay + (e.getValue().delayInv ? "(INVERSE)" : "");
            strobedata = strobedata + (e.getValue().lowZero ? ",DO_LOWZERO" : "") + ")";
            command = command + strobedata;
        }
        String pwmMax;
        for (Map.Entry<String, Integer> e : pwmMaxMap.entrySet()) {
            pwmMax = ";MAX_PWM#" + e.getKey() + ":" + e.getValue();
            command = command + pwmMax;
        }
        if (swtForce.isChecked()) {
            command = command + ";DO_FORCE";
        }
        if (swtTransit.isChecked()) {
            command = command + ";DO_TRANSIT";
        }
        if (swtZero.isChecked()) {
            command = command + ";DO_SETZERO";
        }
        if (swtTime.isChecked()) {
            command = command + ";TIMER(" + editTime.getText().toString() + ")";
        }
        System.out.println(command);
        return command;
    }

    private Map<String, String[]> pin_handles_tree;
    private Map<String, String[]> pin_handles;
    public final static String[] ALLALL = {"ALL", "CAULDRON", "C_LIGHT", "C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_UTIL", "C_PUMPS", "C_PUMP1", "C_PUMP2", "C_EXT", "FIRE", "F_RED", "F_GREEN", "F_BLUE"};
    public final static String[] ALL = {"CAULDRON", "C_LIGHT", "C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_UTIL", "C_PUMPS", "C_PUMP1", "C_PUMP2", "C_EXT", "FIRE", "F_RED", "F_GREEN", "F_BLUE"};
    public final static String[] ALL_PINS = {"C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_PUMP1", "C_PUMP2", "C_EXT", "F_RED", "F_GREEN", "F_BLUE"};
    public final static String[] CAULDRON = {"C_LIGHT", "C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_UTIL", "C_PUMPS", "C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] CAULDRON_PINS = {"C_RED", "C_BLUE", "C_GREEN", "C_WHITE", "C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] C_LIGHT = {"C_RED", "C_BLUE", "C_GREEN", "C_WHITE"};
    public final static String[] C_RED = {"C_RED"};
    public final static String[] C_BLUE = {"C_BLUE"};
    public final static String[] C_GREEN = {"C_GREEN"};
    public final static String[] C_WHITE = {"C_WHITE"};
    public final static String[] C_UTIL = {"C_PUMPS", "C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] C_UTIL_PINS = {"C_PUMP1", "C_PUMP2", "C_EXT"};
    public final static String[] C_PUMPS = {"C_PUMP1", "C_PUMP2"};
    public final static String[] C_PUMP1 = {"C_PUMP1"};
    public final static String[] C_PUMP2 = {"C_PUMP2"};
    public final static String[] C_EXT = {"C_EXT"};
    public final static String[] FIRE = {"F_RED", "F_GREEN", "F_BLUE"};
    public final static String[] F_RED = {"F_RED"};
    public final static String[] F_BLUE = {"F_GREEN"};
    public final static String[] F_GREEN = {"F_BLUE"};

    public Context getActivity() {
        return this;
    }

    private class StrobeValueSet {
        public String high, flash, delay;
        public boolean lowZero, highInv, flashInv, delayInv;

        public StrobeValueSet(String high, String flash, String delay, boolean highInv, boolean flashInv, boolean delayInv, boolean lowZero) {
            this.high = high;
            this.flash = flash;
            this.delay = delay;
            this.highInv = highInv;
            this.flashInv = flashInv;
            this.delayInv = delayInv;
            this.lowZero = lowZero;
        }
    }
}
