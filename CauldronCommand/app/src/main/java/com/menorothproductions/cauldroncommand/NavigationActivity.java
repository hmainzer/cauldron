package com.menorothproductions.cauldroncommand;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class NavigationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        ((Button) findViewById(R.id.btnCommander)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onStartCommanderPress();
            }
        });

        ((Button) findViewById(R.id.btnRgbPicker)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onStartPickerPress();
            }
        });

        ((Button) findViewById(R.id.btnPulseCreator)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onStartPulseCreatorPress();
            }
        });

        ((Button) findViewById(R.id.btnStroboCreator)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onStartStroboCreatorPress();
            }
        });
    }

    private void onStartStroboCreatorPress() {
        Intent i = new Intent(getApplicationContext(), CreateStroboActivity.class);
        startActivity(i);
        finish();
    }

    public void onStartCommanderPress() {
        Intent i = new Intent(getApplicationContext(), ControlActivity.class);
        startActivity(i);
        finish();
    }

    public void onStartPickerPress() {
        Intent i = new Intent(getApplicationContext(), PinPickerActivity.class);
        startActivity(i);
        finish();
    }

    public void onStartPulseCreatorPress() {
        Intent i = new Intent(getApplicationContext(), CreatePulseActivity.class);
        startActivity(i);
        finish();
    }


}
