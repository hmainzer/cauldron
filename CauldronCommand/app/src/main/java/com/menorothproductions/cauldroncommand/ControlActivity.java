package com.menorothproductions.cauldroncommand;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ControlActivity extends AppCompatActivity {

    private Button btnConnect, btnStart, btnStop, btnStopAll, btnStopStart;
    private EditText editIp;
    private Spinner cmdNameSpinner;
    private String serverAddress;
    private ClientThread socketThread;
    private Map<String, String> savedCommands;
    private List<String> cmdNames;
    private String lastStarted = null;



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control);
        socketThread = new ClientThread();
        loadCommands();

        editIp = (EditText) findViewById(R.id.editIP);
        // editPulseName = (EditText) findViewById(R.id.editPulseNameCtrl);

        cmdNameSpinner = (Spinner) findViewById(R.id.cmdNameSpinner);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, cmdNames); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cmdNameSpinner.setAdapter(spinnerArrayAdapter);

        btnConnect = (Button) findViewById(R.id.btnConnect);
        btnStart = (Button) findViewById(R.id.btnStart);
        btnStop = (Button) findViewById(R.id.btnStop);
        btnStopAll = (Button) findViewById(R.id.btnStopAll);
        btnStopStart = (Button) findViewById(R.id.btnStopStart);

        btnConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!socketThread.isConnected()) {
                    serverAddress = editIp.getText().toString();
                    if (!serverAddress.equals("")) {
                        Thread t = new Thread(socketThread);
                        t.start();
                    } else {
                        Log.d("!!!", "No IP!");
                    }
                } else {
                    socketThread.disconnect();
                }
            }
        });

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Context context = getActivity();
                SharedPreferences sharedPref = context.getSharedPreferences("PULSE_SAVE", Context.MODE_PRIVATE);
                String cmd = sharedPref.getString(editPulseName.getText().toString(), "");*/
                String cmd = savedCommands.get(cmdNameSpinner.getSelectedItem().toString());
                if (socketThread.isConnected()) {
                    socketThread.sendCommand(cmd);
                    lastStarted = cmdNameSpinner.getSelectedItem().toString();
                }
            }
        });

        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (socketThread.isConnected()) {
                    String cmd = savedCommands.get(cmdNameSpinner.getSelectedItem().toString());
                    String handle = cmd.substring(cmd.indexOf("#") + 1, cmd.indexOf(";"));
                    socketThread.sendCommand("STOP#" + handle + ":DO_SETZERO");
                }
            }
        });

        btnStopAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (socketThread.isConnected()) {
                    socketThread.sendCommand("STOP#ALL:DO_SETZERO");
                }
            }
        });

        btnStopStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (socketThread.isConnected()) {
                    if (lastStarted != null) {
                        String cmd = savedCommands.get(cmdNameSpinner.getSelectedItem().toString());
                        String handle = cmd.substring(cmd.indexOf("#") + 1, cmd.indexOf(";"));
                        socketThread.sendCommand("STOP#" + handle + ":DO_SETZERO");
                    }
                    String cmd = savedCommands.get(cmdNameSpinner.getSelectedItem().toString());
                    socketThread.sendCommand(cmd);
                    lastStarted = cmdNameSpinner.getSelectedItem().toString();
                }
            }
        });
    }

    private void loadCommands() {
        savedCommands = new HashMap<>();
        cmdNames = new ArrayList<>();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        for (Map.Entry<String, ?> e : preferences.getAll().entrySet()) {
            savedCommands.put(e.getKey(), e.getValue().toString());
            cmdNames.add(e.getKey());
        }
    }

    public static void setDefaults(String key, String value, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    public static void removeDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().remove(key).commit();
    }

    @Override
    public void onBackPressed() {
        socketThread.disconnect();
        Intent i = new Intent(getApplicationContext(), NavigationActivity.class);
        startActivity(i);
        finish();
    }

    public Context getActivity() {
        return this;
    }

    public class ClientThread implements Runnable {

        private Socket socket;
        private boolean connected;

        public void disconnect() {
            connected = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btnConnect.setText("Connect");
                }
            });
        }

        public boolean isConnected() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btnConnect.setText(connected ? "Disconnect" : "Connect");
                }
            });
            return connected;
        }

        public ClientThread() {
            connected = false;
        }

        public void run() {
            try {
                Log.d("ClientActivity", "C: Connecting...");
                socket = new Socket(serverAddress, 12345);
                connected = true;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnConnect.setText("Disconnect");
                    }
                });
                //  connectButton.setText("Disconnect");
                while (connected) {
                    Log.d("ClientActivity", "C: Wait.");
                    Thread.sleep(3000);
                }
                socket.close();
                Log.d("ClientActivity", "C: Closed.");
            } catch (Exception e) {
                Log.e("ClientActivity", "C: Error", e);
                connected = false;
            }
        }

        public boolean sendCommand(String command) {
            if (connected) {
                try {
                    Log.d("ClientActivity", "C: Sending command: " + command);
                    PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket
                            .getOutputStream())), true);
                    out.println(command);
                    Log.d("ClientActivity", "C: Sent.");
                    return true;
                } catch (Exception e) {
                    Log.e("ClientActivity", "S: Error", e);
                    connected = false;
                }
            }
            return false;
        }
    }
/*
    public class ClientThread implements Runnable {

        private Socket socket;
        private boolean connected;

        public void disconnect() {
            connected = false;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btnConnect.setText("Connect");
                }
            });
        }

        public boolean isConnected() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    btnConnect.setText(connected ? "Disconnect" : "Connect");
                }
            });
            return connected;
        }

        public ClientThread() {
            connected = false;
        }

        public void run() {
            try {
                Log.d("ClientActivity", "C: Connecting...");
                connected = true;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnConnect.setText("Disconnect");
                    }
                });
                //  connectButton.setText("Disconnect");
                while (connected) {
                    Log.d("ClientActivity", "C: Wait.");
                    Thread.sleep(3000);
                }
                Log.d("ClientActivity", "C: Closed.");
            } catch (Exception e) {
                Log.e("ClientActivity", "C: Error", e);
                connected = false;
            }
        }

        public boolean sendCommand(String command) {
            if (connected) {
                try {
                    Log.d("ClientActivity", "C: Sending command: " + command);
                    System.out.println(command);
                    Log.d("ClientActivity", "C: Sent.");
                    return true;
                } catch (Exception e) {
                    Log.e("ClientActivity", "S: Error", e);
                    connected = false;
                }
            }
            return false;
        }
*/
}
